(function ($){
	'use strict'

	const header = $('.header')

	function header_sticky() {
		if ($(window).scrollTop() > 100) {
			header.addClass("sticky")
		} else {
			header.removeClass("sticky")
		}
	}

	$(window).scroll(function () {
		header_sticky()
	})

	$(document).ready(function ($) {

		$('.hamburger').click(function(event) {
			event.preventDefault()
			$(this).toggleClass('active')
			header.toggleClass('shown-nav')
		})

		$(".go-top").click(function() {
			$("html, body").animate({ scrollTop: 0 }, "slow")
			return false
		})
	})

})(jQuery)