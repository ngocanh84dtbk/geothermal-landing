</main>
<footer class="footer">
    <div class="container">
        <div class="footer__widget"><p>Geothermal Industries Australia Pty Ltd</p>
            <p>52 Gaine Road<br>South Dandenong<br>VIC 3175</p></div>
        <div class="footer__widget">

            <p>P: 0437 973 583<br>E: info@geothermalindustries.com.au</p>

            <p>ACN 605 463 018<br>ABN 92 605 463 018</p>
        </div>

        <div class="footer__widget">
            <a href="<?php echo home_url() ?>">
                <img src="<?php echo devkudo_image('geothermal-industries-logo.png') ?>"
                     alt="geothermal industries logo">
            </a>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
