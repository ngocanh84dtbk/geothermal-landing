<section class="page__title">
    <div class="container">
        <h1>Natural’s Energy Source</h1>
        <p>Clean, renewable & cost-efficient geothermal energy</p>
    </div>
</section>

<section class="contact">
    <div class="container">
        <div class="contact__text">
            <h2>We’re Proven Geothermal Solutions Experts</h2>
            <ul>
                <li>We employ the most advanced drilling techniques in Australia and have extensive in-house
                    knowledge and experience
                </li>
                <li>Our people are some of the most qualified personnel in the industry</li>
                <li>We have an extensive focus on safety and site management</li>
                <li>Our expertise is proven with leading builders across Australia providing glowing references.
                </li>
            </ul>
            <figure>
                <img src="http://geothermal.test/wp-content/uploads/2021/06/section1.jpg" alt="">
            </figure>
        </div>
        <div class="contact__form">
            <h2>Harness the Earth’s Natural Energy with the Proven Geothermal Performers.</h2>
            <p>Book a FREE* Assessment to receive a tailored recommendation from our Geothermal Solutions
                Experts.</p>
            <?php echo do_shortcode('[contact-form-7 id="23" title="Contact form 1"]'); ?>
        </div>
    </div>
</section>
<section class="break-page">
    <div class="container">
        <div class="break-page__text">
            <h2>Proven Expertise in Geothermal Applications:</h2>
        </div>
        <div class="break-page__content">
            <ul>
                <li>Residential</li>
                <li>Aged Care</li>
                <li>Education</li>
                <li>Industrial and Warehousing</li>
                <li>Other Commercial developments</li>
            </ul>
        </div>
    </div>
</section>