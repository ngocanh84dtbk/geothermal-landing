<?php
/**
 *
 * Return theme image
 *
 * @param $image
 * @return string
 */
function devkudo_image($image) {
	echo get_template_directory_uri()."/assets/images/{$image}";
}

/**
 * Enqueue scripts and styles.
 *
 * @since sunny 1.0.0
 *
 * @return void
 */
function devkudo_scripts() {
	wp_enqueue_style( 'animate-style', '//cdn.rawgit.com/daneden/animate.css/v3.1.0/animate.min.css', null, '3.1.0' );
	wp_enqueue_style( 'sunny-style', get_template_directory_uri() . '/assets/css/styles.css', array(), wp_get_theme()->get( 'Version' ) );
	wp_enqueue_script( 'wow', '//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', array(), '1.1.2', true );
	wp_enqueue_script( 'geothermal-common-script', get_template_directory_uri() . '/assets/js/common.js', array( 'jquery' ), wp_get_theme()->get( 'Version' ),  true);
}
add_action( 'wp_enqueue_scripts', 'devkudo_scripts' );

/**
 * Add theme support wide
 */
add_theme_support ('align-wide');

