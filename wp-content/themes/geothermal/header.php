<!DOCTYPE html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@400;600;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class();?>>
<header class="header">
    <div class="container">
        <a href="<?php echo home_url()?>">
            <figure>
                <img src="<?php echo devkudo_image('geothermal-industries-logo.png')?>" alt="geothermal industries logo">
            </figure>
        </a>
    </div>
</header>
<main class="container">